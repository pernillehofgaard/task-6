﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Task6
{
	public class Person
	{
		public string firstName { get; set; }
		public string lastName { get; set; }
		public long telephone { get; set; }

		public Person(string firstName, string lastName, long telephone)
		{
			this.firstName = firstName;
			this.lastName = lastName;
			this.telephone = telephone;
		}

		public bool FindPerson(string name)
		{
			bool found = false;
			if(name == firstName || name == this.lastName)
			{
				found = true;
			   
			}
			else
			{
				found = false;
			}

			return found;
		}

		public override string ToString()
		{
			return this.firstName + " " + this.lastName;
		}
	}
}
