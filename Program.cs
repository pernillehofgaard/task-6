﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading;

namespace Task6
{
	class Program
	{
		static List<Person> persons = new List<Person>();
		
		static void Main(string[] args)
		{
			Person person1 = new Person("NICK", "LENNOX", 12345678);
			Person person2 = new Person("PERNILLE", "HOFGAARD", 46887876);
			Person person3 = new Person("KARI", "NORDMANN", 87654321);
			Person person4 = new Person("OLA", "NORDMANN", 23456789);
			Person person5 = new Person("DEWALND", "ELS", 98765432);
			persons.Add(person1);
			persons.Add(person2);
			persons.Add(person3);
			persons.Add(person4);
			persons.Add(person5);


			Console.WriteLine("Who are you looking for?");
			string name = Console.ReadLine().ToUpper();
			int count = 0;

			foreach(var Person in persons)
            {
				bool exists = Person.FindPerson(name);

				if (exists)
                {
                    Console.WriteLine(Person.firstName + " " + Person.lastName);
                }     
            }			
		}
	}
}
